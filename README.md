# HH_bbtautau_compare_and_plot



## Getting started

This is a tool designed initially for the LepHad channel of the H(bb)H(tautau) analysis with ATLAS. As it is, the code compares outputs of the CxAODReader and can plot various variables and samples with ratios. The aim is to have a flexible code to be tailored to your needs, and you can see various files with similar functionalities and enough documentation to get you started.