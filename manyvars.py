#!/usr/bin/env python
import sys
from ROOT import *
import os
import math
import glob
import argparse
from lib import Variables


"""
Usage:
	python compare.py -f "<your filezzz to be compared, separated by :>" -s "<samples (or one sample) of the files (in order), separated by :, grouped by ,>" -v "<variables (or one variable) from files (in order), separated by :>" -d "<name of output directory>" -e "<explanations written on the plot>"
"""
parser = argparse.ArgumentParser(description='Process output file from evaluation for plots')
parser.add_argument('-f', "--Files", dest='Files', default='/data/atlas/users/okarkout/SLT/SLT_withdata.root', help='your filezzz to be compared, separated by :')
parser.add_argument('-s', "--samples", dest='samples', default='Fake:ttbarFake,WFake,WttFake', help='samples (or one sample) of the files (in order), separated by :, grouped by ,')
parser.add_argument('-v', "--variables", dest='variables', default='MET', help='variables (or one variable) from files (in order), separated by :')
parser.add_argument('-d', "--outDir", dest='outDir', default='', help='Directory to store the plots')
parser.add_argument('-e', "--Explanation", dest='Explanation', default='Data Fakes vs MC Fakes', help='Explanation of the region, to show on y axis.') # for example, "Data Fakes over MC Fakes"
args = parser.parse_args()

#To improve:
# fix: numbers on X axis main plot are half covered
# automate range of x and y axes
#+logplot
def main():
	Trees = []
	Names = []
	for p in (args.Files).split(":"):
		Name = p.split("/")[-1].replace("_withdata.root", "")
		f = TFile.Open(p, "READ")
		t = f.Get("Nominal")
		Trees.append(t)
		Names.append(Name)

	Vars = (args.variables).split(":")

	Samples = (args.samples).split(":")

	drawTwoVars(Trees, Vars, Samples, args.Explanation, Names)

	f.Close()

def drawTwoVars(Trees, Vars, Samples, Explain, Names):
	#hists:
	t1 = Trees[0]
	v1 = Vars[0]
	s1 = Samples[0]
	Name1 = Names[0]
	
	try:
		t2 = Trees[1]
	except:
		print('using only one tree:')
		t2 = t1
		print(t1)
	try:
		v2 = Vars[1]
	except:
		print('using only one variable:')
		v2 = v1
		print(v1)
	try:
		s2 = Samples[1]
	except:
		print('using only one sample set')
		s2 = s1
	try:
		Name2 = Names[1]
	except:
		print('using only one file')
		Name2 = Name1
	
	set1 = ''
	set2 = ''
	for sam in s1.split(','):
		set1 += 'sample == \"%s\" | '%sam
	for sam in s2.split(','):
		set2 += 'sample == \"%s\" | '%sam

	print(set1[:-3], set2[:-3])
	unit1, bin1, start1, end1 = Variables[v1][0], Variables[v1][1][0], Variables[v1][1][1], Variables[v1][1][2]
	unit2, bin2, start2, end2 = Variables[v2][0], Variables[v2][1][0], Variables[v2][1][1], Variables[v2][1][2]

	N_events1 = t1.GetEntries("(" + set1[:-3] + ") * ( OS == 1  && mBB < 150000)")
	N_events2 = t2.GetEntries("(" + set2[:-3] + ") * ( OS == 1  && mBB < 150000)")
	
	h1 =  TH1F("h1", Name1 + ": " "Data Fakes over MC Fakes", bin1, start1, end1)
	h2 =  TH1F("h2", Name2 + ": " "Data Fakes over MC Fakes", bin2, start2, end2)
	
	setAxisStyle(h1, v1 + '   (' + unit1 +')' , " a.u. (normalised)")
	setAxisStyle(h2, v2 + '   (' + unit2 +')' , " a.u. (normalised)")

	t1.Project("h1", v1, "weight * (" + set1[:-3] + ") * ( OS == 1  && mBB < 150000 )")
	t2.Project("h2", v2, "weight * (" + set2[:-3] + ") * ( OS == 1  && mBB < 150000 )")

	h1.Scale(1.0 / h1.Integral())
	h2.Scale(1.0 / h2.Integral())
	hist_1over2 = getSigBkgCompare(h1, h2)
	hist_1over1 = getSigBkgCompare(h1, h1)

	h1.SetDirectory(0)
	h2.SetDirectory(0)
	hist_1over2.SetDirectory(0)
	hist_1over1.SetDirectory(0)

	# Build legends
	legend = TLegend(0.55,0.8,0.85,0.9)
	legend.AddEntry(h1, s1 + ': ' + str(N_events1), "l")
	legend.AddEntry(h2, s2 + ': ' + str(N_events2), "l")

	# The canvas
	c1 = TCanvas("c1", "c1", 800, 600)
	c1.cd()
	mainPad = TPad("main", "main", 0, 0.3, 1, 1)
	ratioPad = TPad("ratio", "ratio", 0, 0, 1, 0.3)
	configurePads(mainPad, ratioPad)
	mainPad.Draw()
	ratioPad.Draw()
	gStyle.SetOptStat(0) # No stats.


	mainPad.cd()
	h1.Draw()
	h2.SetLineColor(kRed)
	h2.Draw("same")
	legend.Draw("same")
	ltx = TLatex()
	ltx.SetNDC()
	# ltx.SetTextSize(0.025)

	ratioPad.cd()
	hist_1over2.Draw()
	hist_1over1.SetLineColor(kBlack)
	hist_1over1.Draw('same')
	# hist_Significance.Draw("hist e same")

	# legratio = TLegend(0.15,0.65,0.45,0.85)
	# legratio.AddEntry(hist_1over2, "%d#times S/B"%int(1), "l")
	# # legratio.AddEntry(hist_Significance, "%d#times Significance"%int(SFSignificance), "l")
	# legratio.Draw("same")
	# # ratioMax = max(hist_1over2.GetMaximum(), hist_Significance.GetMaximum())
	# # setRatioStyle(hist_1over2, "BDT output", "S,B compare", 0., ratioMax*1.3)
	# # setRatioStyle(hist_1over2, "test output", "ratio compare", 0., hist_1over2.GetMaximum() * 1.3)
	setRatioStyle(hist_1over2, str(v1) + unit1, "ratio    ", 0.0, 2)

	plotname = args.outDir + Name1 + '_' + args.variables + '__' + s1 + '_vs_' + s2.replace(',', '_') + ".pdf"
	c1.SaveAs(plotname, "recreate")
	mainPad.SetLogy()
	plotname = args.outDir + Name1 + '_Log_' + args.variables + '__' + s1 + '_vs_' + s2.replace(',', '_') + ".pdf"
	c1.SaveAs(plotname, "recreate")


	h1.Delete()
	h2.Delete()
	# hist_bkg.Delete()
	hist_1over2.Delete()
	# hist_Significance.Delete()
	mainPad.Close()
	ratioPad.Close()
	c1.Close()

def getSigBkgCompare(hist_sig, hist_bkg):
	h_SoverB = hist_sig.Clone()
	h_SoverB.Reset()

	Significance_value = 0.0

	for i in range(1, hist_sig.GetNbinsX()+1):
		SoverB = 0.0
		bin_Significance = 0.0

		nSig = hist_sig.GetBinContent(i)
		nBkg = hist_bkg.GetBinContent(i)
		# print("Bin%d, S: %f, B: %f"%(i, nSig, nBkg))
		if nBkg>1e-6:
			SoverB = nSig/nBkg
		# print("Bin: %d, S/B: %f"%(i, SoverB))
		h_SoverB.SetBinContent(i, SoverB)
	return h_SoverB
#to configure the upper ("upper") and lower ("lower") Pad in a ratio plot
def configurePads(upper, lower) :
	upper.SetFrameFillColor(0)
	upper.SetFrameFillStyle(0)
	upper.SetFillColor(0)
	upper.SetFillStyle(0)
	upper.SetBottomMargin(0.03/(1-(0.4)))
	lower.SetFrameFillColor(0)
	lower.SetFrameFillStyle(0)
	lower.SetFillColor(0)
	lower.SetFillStyle(0)
	lower.SetTopMargin(0.03/(0.4))
	lower.SetBottomMargin(0.3)
	lower.SetTicky()
#to configure the style of the ratio plot for a given histogram (h_down)
def setRatioStyle(h_down, titleX, titleY, yMin, yMax):
	h_down.GetYaxis().SetNdivisions(503,kTRUE)
	h_down.GetYaxis().SetRangeUser(yMin,yMax)
	h_down.GetYaxis().SetLabelSize(0.105)
	h_down.GetYaxis().SetTitleSize(0.105)
	h_down.GetYaxis().SetTitle(titleY)          
	h_down.GetXaxis().SetTitle(titleX)
	h_down.GetYaxis().SetTitleOffset(0.5)            
	h_down.GetXaxis().SetLabelSize(0.105)
	h_down.GetXaxis().SetTitleOffset(1.4)            
	h_down.GetXaxis().SetTitleSize(0.105)
	h_down.SetTitle("")
	gPad.SetTicky()

def setAxisStyle(hist, titleX, titleY):
	hist.GetXaxis().SetLabelSize(0.05)
	hist.GetYaxis().SetLabelSize(0.05)
	if(titleY!="no"):
		hist.GetYaxis().SetTitleSize(0.05)
		hist.GetYaxis().SetTitle(titleY)
		hist.GetYaxis().SetTitleOffset(1.0) 
	if(titleY=="no"):
		hist.GetYaxis().SetTitleSize(0)
		hist.GetYaxis().SetLabelSize(0)
	if(titleX!="no"):           
		hist.GetXaxis().SetTitle(titleX) 
		hist.GetXaxis().SetTitleOffset(1.4)
		hist.GetXaxis().SetTitleSize(0.05) 
	if(titleX=="no"):
		hist.GetXaxis().SetTitleSize(0)
		hist.GetXaxis().SetLabelSize(0)
	# hist.SetTitle("")

if __name__ == "__main__":
	main()

