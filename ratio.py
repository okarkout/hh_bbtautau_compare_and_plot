#!/usr/bin/env python
import sys
from ROOT import *
import os
import math
import glob


#To improve:
# automate range of x axis
def main():
	# pathList = sorted(glob.glob("/data/atlas/users/okarkout/LTT/*/*.root"))
	path = "/data/atlas/users/okarkout/LTT/LTT_training.root"

	f = TFile.Open(path, "UPDATE")
	t1 = f.Get("Nominal")


	# h1 = TH1F("h1", "mTW", 100, 0, 300000)
	# h2 = TH1F("h2", "mTW", 100, 0, 300000)
	# h3 = TH1F("h3", "mTW", 100, 0, 300000)

	# t1.Project("h1", "mTW", "weight * (MET > 20000)")
	# t1.Project("h1", "mTW", "weight")
	# # t1.Project("h2", "mTW", "(MET > 20000)")
	# t1.Project("h3", "mTW")

	# h1.SetDirectory(0)
	# h2.SetDirectory(0)
	# h3.SetDirectory(0)
	drawTwoVars(t1, t1, "MET", "MET", "ttbar", "ttbarFake", "test")

	f.Close()


def drawTwoVars(Tree1, Tree2, Var1, Var2, sample1, sample2, Explain):
	#hists:
	N_events1 = Tree1.GetEntries(Var1)
	N_events2 = Tree2.GetEntries(Var2)
	h1 =  TH1F("h1", "Compare " + Var1 + " of " + sample1 +  " and " + Var2 + " of " + sample2, 100, 0, 100000)
	h2 =  TH1F("h2", "Compare " + Var1 + " of " + sample1 +  " and " + Var2 + " of " + sample2, 100, 0, 100000)
	Tree1.Project("h1", Var1, "weight * (sample == \"%s\")"%sample1)
	Tree2.Project("h2", Var2, "weight * (sample == \"%s\")"%sample2)

	hist_1over2, _, _ = getSigBkgCompare(h1, h2)

	h1.SetDirectory(0)
	h2.SetDirectory(0)
	hist_1over2.SetDirectory(0)

	# Build legends
	legend = TLegend(0.55,0.8,0.85,0.9)
	legend.AddEntry(h1, sample1 + ': ' + str(N_events1), "l")
	legend.AddEntry(h2, sample2 + ': ' + str(N_events2), "l")

	# The canvas
	c1 = TCanvas("c1", "c1", 800, 600)
	c1.cd()
	mainPad = TPad("main", "main", 0, 0.3, 1, 1)
	ratioPad = TPad("ratio", "ratio", 0, 0, 1, 0.27)
	configurePads(mainPad, ratioPad)
	mainPad.Draw()
	ratioPad.Draw()
	gStyle.SetOptStat(1) # No stats.

	mainPad.cd()
	h1.Scale(1.0 / h1.Integral())
	h1.Draw()
	h2.SetLineColor(kRed)
	h2.Scale(1.0 / h2.Integral())
	h2.Draw("same")
	legend.Draw("same")
	ltx = TLatex()
	ltx.SetNDC()
	# ltx.SetTextSize(0.02)
	# ltx.DrawLatex(0.12, 0.85, Explain)

	ratioPad.cd()
	hist_1over2.Draw()
	# hist_Significance.Draw("hist e same")

	# legratio = TLegend(0.15,0.65,0.45,0.85)
	# legratio.AddEntry(hist_1over2, "%d#times S/B"%int(1), "l")
	# # legratio.AddEntry(hist_Significance, "%d#times Significance"%int(SFSignificance), "l")
	# legratio.Draw("same")
	# # ratioMax = max(hist_1over2.GetMaximum(), hist_Significance.GetMaximum())
	# # setRatioStyle(hist_1over2, "BDT output", "S,B compare", 0., ratioMax*1.3)
	# # setRatioStyle(hist_1over2, "test output", "ratio compare", 0., hist_1over2.GetMaximum() * 1.3)
	setRatioStyle(hist_1over2, "test output", "ratio compare", 0, 2)

	# plotname = ""
	# if Trafo:
	#     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_TrafoD.pdf"
	# else: 
	#     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
	# plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
	mainPad.SetLogy()
	plotname = "output.pdf"

	c1.SaveAs(plotname, "recreate")

	h1.Delete()
	h2.Delete()
	# hist_bkg.Delete()
	hist_1over2.Delete()
	# hist_Significance.Delete()
	mainPad.Close()
	ratioPad.Close()
	c1.Close()

def getSigBkgCompare(hist_sig, hist_bkg):
	h_SoverB = hist_sig.Clone()
	h_SoverB.Reset()

	h_Significance = hist_sig.Clone()
	h_Significance.Reset()

	Significance_value = 0.0

	for i in range(1, hist_sig.GetNbinsX()+1):
		SoverB = 0.0
		bin_Significance = 0.0

		nSig = hist_sig.GetBinContent(i)
		nBkg = hist_bkg.GetBinContent(i)
		# print("Bin%d, S: %f, B: %f"%(i, nSig, nBkg))
		if nBkg>1e-6:
			SoverB = nSig/nBkg
			bin_Significance = 2.0*((nSig+nBkg)*math.log(1+nSig/nBkg)-nSig)
			Significance_value += bin_Significance
			bin_Significance = math.sqrt(bin_Significance)
		# print("Bin: %d, S/B: %f"%(i, SoverB))
		# print("Bin%d, Significance: %f"%(i, bin_Significance))
		h_SoverB.SetBinContent(i, SoverB)
		h_Significance.SetBinContent(i, bin_Significance)
	Significance_value = math.sqrt(Significance_value)
	return h_SoverB, h_Significance, Significance_value
#to configure the upper ("upper") and lower ("lower") Pad in a ratio plot
def configurePads(upper, lower) :
	upper.SetFrameFillColor(0)
	upper.SetFrameFillStyle(0)
	upper.SetFillColor(0)
	upper.SetFillStyle(0)
	upper.SetBottomMargin(0.02/(1-(0.3+0.02)))
	lower.SetFrameFillColor(0)
	lower.SetFrameFillStyle(0)
	lower.SetFillColor(0)
	lower.SetFillStyle(0)
	lower.SetTopMargin( -0.1 + 0.02/(0.3+0.02))
	lower.SetBottomMargin(0.3)
	lower.SetTicky()
#to configure the style of the ratio plot for a given histogram (h_down)
def setRatioStyle(h_down, titleX, titleY, yMin, yMax):
	h_down.GetYaxis().SetNdivisions(503,kTRUE)
	h_down.GetYaxis().SetRangeUser(yMin,yMax)
	h_down.GetYaxis().SetLabelSize(0.105)
	h_down.GetYaxis().SetTitleSize(0.105)
	h_down.GetYaxis().SetTitle(titleY)          
	h_down.GetXaxis().SetTitle(titleX)
	h_down.GetYaxis().SetTitleOffset(0.5)            
	h_down.GetXaxis().SetLabelSize(0.105)
	h_down.GetXaxis().SetTitleOffset(1.4)            
	h_down.GetXaxis().SetTitleSize(0.105)
	h_down.SetTitle("")
	gPad.SetTicky()



if __name__ == "__main__":
	main()

