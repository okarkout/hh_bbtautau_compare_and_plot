#!/usr/bin/env python
import sys
import ROOT
import os
import math

from array import array
from styleConfigurations import configureHisto
from styleConfigurations import configurePads
from styleConfigurations import setRatioStyle
from styleConfigurations import setAxisStyle
import argparse


"""
Usage:
    From "run" directory, do
    python ../source/MVATraining_VHbb/scripts/drawEvalOutput.py -f "<your evaluation file>" -d "<name of output directory>" -z "<evaluation regions>" -e "<explanations of each region>"
"""
parser = argparse.ArgumentParser(description='Process output file from evaluation for plots')
parser.add_argument('-f', "--Files", dest='Files', default='', help='Output file from evaluation')
parser.add_argument('-d', "--outDir", dest='outDir', default='', help='Directory to store the plots')
parser.add_argument('-z', "--AnalysisRegions", dest='AnalysisRegions', help='Regions on which the evaluation is done. separated by :.') # for example, "j2low", used for the plot name, don't put any space in between
parser.add_argument('-e', "--Explanation", dest='Explanation', default='', help='Explanation of the region, to show on legends, separated by :.') # for example, "2 jets, low pTV"
parser.add_argument('-s', "--SF", dest='SF', default='0', help='0: Use default scale factors for S/B & Significance hists (10*Significance, 100*S/B), Any other char/str: calculate the SFs on the fly')
args = parser.parse_args()

def getScale(h_higher, h_lower):
    ratio = 1
    ratio = int(h_higher.GetMaximum()/h_lower.GetMaximum())
    SFhigh = 1
    if h_higher.GetMaximum()<1 and h_higher.GetMaximum()>1e-10:
        SFhigh = int(1.0/h_higher.GetMaximum())
    SFlow = 1.0*SFhigh*ratio
    SFhigh = 1.0*SFhigh
    return SFhigh, SFlow


def main():
    ROOT.gROOT.SetBatch(True)

    print("==================================")
    print("==> Plotting outputs of Evaluation")
    print("==================================")

    # get the evaluation file
    EvalFilename = args.Files
    EvalFile = ROOT.TFile(EvalFilename)

    # get the regions of evaluation
    VarList = (args.AnalysisRegions).split(":")
    num_EvalRegion = len(VarList)

    # get the explanations of regions
    ExplainList = (args.Explanation).split(":")

    # make directory for saving
    outDirName = args.outDir + "/"
    if not (os.path.isdir(outDirName)):
        print("Creating directory \t", outDirName)
        os.mkdir(outDirName)

    for i in VarList:
        Var = str(i)
        h_sig = EvalFile.Get("Nominal")
        # h_bkg = EvalFile.Get("bg_region"+region+"_mva")
        # h_sig.Rebin(25)
        # h_bkg.Rebin(25)
        # h_sig_trafoD = EvalFile.Get("sig_region"+region+"_mva_trafoD")
        # h_bkg_trafoD = EvalFile.Get("bg_region"+region+"_mva_trafoD")
        # h_list = [h_sig, h_bkg, h_sig_trafoD, h_bkg_trafoD]

        h_SoverB, _, _ = getSigBkgCompare(h_sig, h_sig) # h_SoverB_notrafo, h_Significance_notrafo, Significance_notrafo = getSigBkgCompare(h_sig, h_bkg)
        # h_SoverB_trafo, h_Significance_trafo, Significance_trafo = getSigBkgCompare(h_sig_trafoD, h_bkg_trafoD)

        # if args.SF=='0':
        #     SFSoverB = 100.0
        #     SFSignificance = 10.0
        #     SFSoverB_trafo = 100.0
        #     SFSignificance_trafo = 10.0
        # else:
        #     if h_SoverB_notrafo.GetMaximum()>h_Significance_notrafo.GetMaximum():
        #         SFSoverB, SFSignificance = getScale(h_SoverB_notrafo, h_Significance_notrafo)
        #     else:
        #         SFSignificance, SFSoverB = getScale(h_Significance_notrafo, h_SoverB_notrafo)
        #     if h_SoverB_trafo.GetMaximum()>h_Significance_trafo.GetMaximum():
        #         SFSoverB_trafo, SFSignificance_trafo = getScale(h_SoverB_trafo, h_Significance_trafo)
        #     else:
        #         SFSignificance_trafo, SFSoverB_trafo = getScale(h_Significance_trafo, h_SoverB_trafo)

        # h_SoverB_notrafo.Scale(SFSoverB)
        # h_SoverB_trafo.Scale(SFSoverB_trafo)
        # h_Significance_notrafo.Scale(SFSignificance)
        # h_Significance_trafo.Scale(SFSignificance_trafo)

        # # normalisation
        # for h in h_list:
        #     print("Integral value = %f"%h.Integral(0, -1))
        #     h.Scale(1.0/h.Integral(0, -1))
        #     print("Integral value after normalisation = %f"%h.Integral(0,-1))

        # drawEvalOutput(h_sig, h_bkg, h_SoverB_notrafo, h_Significance_notrafo, SFSoverB, SFSignificance, EvalRegionList[i], ExplainList[i], False)
        # drawEvalOutput(h_sig_trafoD, h_bkg_trafoD, h_SoverB_trafo, h_Significance_trafo, SFSoverB_trafo, SFSignificance_trafo, EvalRegionList[i], ExplainList[i], True)
        drawOneVar(h_sig, h_SoverB)


def drawOneVar(hist_sig, hist_SoverB, Var):
    # Set histograms
    setAxisStyle(hist_sig, "no", "not normalised")

    configureHisto(hist_sig, 597, 1, 1, 0, 2, 0)
    configureHisto(hist_SoverB, 418, 1, 1, 0, 2, 0)

    # Build legends
    legend = ROOT.TLegend(0.55,0.8,0.85,0.9)
    legend.AddEntry(hist_sig, "Signal", "l")

    # The canvas
    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    c1.cd()
    mainPad = ROOT.TPad("main", "main", 0, 0.3, 1, 1)
    ratioPad = ROOT.TPad("ratio", "ratio", 0, 0, 1, 0.3)
    configurePads(mainPad, ratioPad)
    mainPad.Draw()
    ratioPad.Draw()
    ROOT.gStyle.SetOptStat(0) # No stats.

    mainPad.cd()
    hist_sig.Draw(Var) # hist_bkg.Draw("hist e")
    hist_sig.Draw(Var + " same")
    legend.Draw("same")
    ltx = ROOT.TLatex()
    ltx.SetNDC()
    ltx.SetTextSize(0.05)
    ltx.DrawLatex(0.12, 0.85, Explain)

    ratioPad.cd()
    hist_SoverB.Draw("hist e")
    # hist_Significance.Draw("hist e same")

    legratio = ROOT.TLegend(0.15,0.65,0.45,0.85)
    legratio.AddEntry(hist_SoverB, "%d#times S/B"%int(SFSoverB), "l")
    # legratio.AddEntry(hist_Significance, "%d#times Significance"%int(SFSignificance), "l")
    legratio.Draw("same")
    # ratioMax = max(hist_SoverB.GetMaximum(), hist_Significance.GetMaximum())
    # setRatioStyle(hist_SoverB, "BDT output", "S,B compare", 0., ratioMax*1.3)
    setRatioStyle(hist_SoverB, "BDT output", "S,B compare", 0., hist_SoverB.GetMaximum() * 1.3)

    # plotname = ""
    # if Trafo:
    #     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_TrafoD.pdf"
    # else: 
    #     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
    # plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
    plotname = "/BDTouput_" + Var + ".pdf"

    c1.SaveAs(plotname, "recreate")

    hist_sig.Delete()
    # hist_bkg.Delete()
    hist_SoverB.Delete()
    # hist_Significance.Delete()
    mainPad.Close()
    ratioPad.Close()
    c1.Close()

def drawEvalOutput(hist_sig, hist_bkg, hist_SoverB, hist_Significance, SFSoverB, SFSignificance, EvalRegion, Explain, Trafo = False):
    # Set histograms
    setAxisStyle(hist_bkg, "no", "a.u. (normalised)")
    setAxisStyle(hist_sig, "no", "a.u. (normalised)")

    configureHisto(hist_sig, 597, 1, 1, 0, 2, 0)
    configureHisto(hist_bkg, 613, 1, 1, 0, 2, 0)
    configureHisto(hist_SoverB, 418, 1, 1, 0, 2, 0)
    configureHisto(hist_Significance, 801, 1, 1, 0, 2, 0)

    hist_bkg.SetMaximum(max(hist_sig.GetMaximum(), hist_bkg.GetMaximum())*1.3)
    hist_bkg.GetXaxis().SetRangeUser(-1., 1.)

    # Build legends
    legend = ROOT.TLegend(0.55,0.8,0.85,0.9)
    legend.AddEntry(hist_sig, "Signal", "l")
    legend.AddEntry(hist_bkg, "Background", "l")

    # The canvas
    c1 = ROOT.TCanvas("c1", "c1", 800, 600)
    c1.cd()
    mainPad = ROOT.TPad("main", "main", 0, 0.3, 1, 1)
    ratioPad = ROOT.TPad("ratio", "ratio", 0, 0, 1, 0.3)
    configurePads(mainPad, ratioPad)
    mainPad.Draw()
    ratioPad.Draw()
    ROOT.gStyle.SetOptStat(0) # No stats.

    mainPad.cd()
    hist_bkg.Draw("hist e")
    hist_sig.Draw("hist e same")
    legend.Draw("same")
    ltx = ROOT.TLatex()
    ltx.SetNDC()
    ltx.SetTextSize(0.05)
    ltx.DrawLatex(0.12, 0.85, Explain)

    ratioPad.cd()
    hist_SoverB.Draw("hist e")
    hist_Significance.Draw("hist e same")

    legratio = ROOT.TLegend(0.15,0.65,0.45,0.85)
    legratio.AddEntry(hist_SoverB, "%d#times S/B"%int(SFSoverB), "l")
    legratio.AddEntry(hist_Significance, "%d#times Significance"%int(SFSignificance), "l")
    legratio.Draw("same")
    ratioMax = max(hist_SoverB.GetMaximum(), hist_Significance.GetMaximum())
    setRatioStyle(hist_SoverB, "BDT output", "S,B compare", 0., ratioMax*1.3)

    plotname = ""
    if Trafo:
        plotname = args.outDir + "/BDTouput_" + EvalRegion + "_TrafoD.pdf"
    else: 
        plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"

    c1.SaveAs(plotname, "recreate")

    hist_sig.Delete()
    hist_bkg.Delete()
    hist_SoverB.Delete()
    hist_Significance.Delete()
    mainPad.Close()
    ratioPad.Close()
    c1.Close()


def getSigBkgCompare(hist_sig, hist_bkg):
    h_SoverB = hist_sig.Clone()
    h_SoverB.Reset()

    h_Significance = hist_sig.Clone()
    h_Significance.Reset()

    Significance_value = 0.0

    for i in range(1, hist_sig.GetNbinsX()+1):
        SoverB = 0.0
        bin_Significance = 0.0

        nSig = hist_sig.GetBinContent(i)
        nBkg = hist_bkg.GetBinContent(i)
        print("Bin%d, S: %f, B: %f"%(i, nSig, nBkg))
        if nBkg>1e-6:
            SoverB = nSig/nBkg
            bin_Significance = 2.0*((nSig+nBkg)*math.log(1+nSig/nBkg)-nSig)
            Significance_value += bin_Significance
            bin_Significance = math.sqrt(bin_Significance)
        print("Bin: %d, S/B: %f"%(i, SoverB))
        print("Bin%d, Significance: %f"%(i, bin_Significance))
        h_SoverB.SetBinContent(i, SoverB)
        h_Significance.SetBinContent(i, bin_Significance)
    Significance_value = math.sqrt(Significance_value)
    return h_SoverB, h_Significance, Significance_value


if __name__ == "__main__":
    main()

