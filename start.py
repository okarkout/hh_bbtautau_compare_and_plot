from ROOT import *
import glob


pathList = sorted(glob.glob("/data/atlas/users/okarkout/LTT/*/*.root"))
path = "/data/atlas/users/okarkout/LTT/LTT_training.root"

f = TFile.Open(path, "UPDATE")
t1 = f.Get("Nominal")
t2 = f.Get("Nominal")

h1 = TH1F("h1", "mTW", 100, 0, 500000)
t1.Draw("mTW>>h1")
h1.SetDirectory(0)

h2 = TH1F("h2", "MET", 100, 0, 500000)
t2.Draw("MET>>h2")
h2.SetDirectory(0)

f.Close()

canv = TCanvas()
h1.Scale(6.0 / h1.Integral())
h1.Draw()

h2.Scale(6.0 / h2.Integral())
h2.SetLineColor(kRed)
h2.Draw("same")
canv.SaveAs("Ploth.pdf")

#To improve: style of histograms (continuous line), ratio plot (from drawEval)

# canv = TCanvas()
# canv.cd()
# t1.Draw("MET")
# t1.SetLineColor(kBlue)
# t2.Draw("same mTW")
# # t2.Draw("mTW")
# t2.SetLineColor(kRed)
# canv.SaveAs("Plot.pdf", "recreate") #only plots mTW

# t.Draw("MET:mTW")
# canv.SaveAs("Plot2.pdf")




# canv = TCanvas()
# t.Draw("MET")
# t2.Draw("mTW")

# canv.SaveAs("Plot.pdf") #only plots mTW

# t.Draw("MET:mTW")
# canv.SaveAs("Plot2.pdf")



#No NEED# can2 = TCanvas()
# t.Draw("MET:mTW")
# can2.SaveAs("plot3.pdf")

# canv.Draw()

# g = gROOT.FindObject("Nominal")
# s2 =  g.Print()


# print(s == s2) #True

# sample = std.string()
# b = t.Branch("sample", sample)
# b.Print()