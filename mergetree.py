# import ROOT
from ROOT import *
import glob

# pathList = sorted(glob.glob("/afs/cern.ch/work/o/okarkout/forCarloAndOsama/LHNtuples/DATA1*_ntuples/hh_bbtt.root"))
pathList = sorted(glob.glob("/data/atlas/users/okarkout/SLT/*/*.root"))

treeList = TList()
# outputFile = TFile('test_merge.root', 'recreate')
outputFile = TFile('/data/atlas/users/okarkout/SLT/SLT_withdata.root', 'recreate')

pyfilelist = []
pytreelist = []

for path in pathList:
        print("Path", path)
        filename = path.split("/")[-1] #ttbar.root or ttbar-0.root
        key = filename.split(".")[0] #ttbar-1
        key = key.split("-")[0] #ttbar

        # if "data" in key:
        #         print('skipping this file: ', key)
        #         continue

        inputFile = TFile(path, 'read')
        pyfilelist.append(inputFile) # Make this TFile survive the loop!
        inputTree = inputFile.Get('Nominal')
        pytreelist.append(inputTree) # Make this TTree survive the loop!
        #outputTree = inputTree.CloneTree() #instead of extensive processing
        treeList.Add(inputTree)

outputFile.cd()
outputTree = TTree.MergeTrees(treeList)
outputFile.Write()
outputFile.Close()