#!/usr/bin/env python
import os
from lib import Variables

# outDir="/project/atlas/users/okarkout/hhbbtt/plottting/data_vs_MC_Fakes_plots/"
# infiles="/data/atlas/users/okarkout/LTT/LTT_withdata.root"
infiles="/data/atlas/users/okarkout/LTT/LTT_withdata.root:/data/atlas/users/okarkout/SLT/SLT_withdata.root"
samp = 'Fake'
ex = "'Data Fakes for SLT over LTT'"
norm = '1'

v = 'MET'
os.system('python plot_vars.py -f %s -s %s -v %s -e %s -n %s'%(infiles, samp, v, ex, norm))

# for v in Variables:
# 	os.system('python manyvars.py -v %s -d %s'%(v, outDir))
# 	os.system('python manyvars.py -f %s -v %s -d %s'%(infiles, v, outDir))
