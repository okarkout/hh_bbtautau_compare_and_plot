#!/usr/bin/env python
import os
from lib import Variables

outDir="/project/atlas/users/okarkout/hhbbtt/plottting/lumi_data_vs_MC_Fakes_plots/"
LTT_file="/data/atlas/users/okarkout/LTT/LTT_withdata.root"

# v = 'MET'
# os.system('python manyvars_lumi.py -v %s'%v)

for v in Variables:
	os.system('python manyvars_lumi.py -v %s -d %s'%(v, outDir))
	os.system('python manyvars_lumi.py -f %s -v %s -d %s'%(LTT_file, v, outDir))