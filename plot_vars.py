#!/usr/bin/env python
from ROOT import *
import math
import argparse
from lib import Variables


"""
Usage:
	python compare.py -f "<your filezzz to be compared, separated by :>" -s "<samples (or one sample) of the files (in order), separated by :, grouped by ,>" -v "<variables (or one variable) from files (in order), separated by :>" -d "<name of output directory>" -e "<explanations written on the plot>"
"""
parser = argparse.ArgumentParser(description='Process output file from evaluation for plots')
parser.add_argument('-f', "--Files", dest='Files', default='/data/atlas/users/okarkout/SLT/SLT_withdata.root', help='your filezzz to be compared, separated by :')
parser.add_argument('-s', "--samples", dest='samples', default='Fake:ttbarFake,WFake,WttFake', help='samples (or one sample) of the files (in order), separated by :, grouped by ,')
parser.add_argument('-v', "--variables", dest='variables', default='MET', help='variables (or one variable) from files (in order), separated by :')
parser.add_argument('-d', "--outDir", dest='outDir', default='', help='Directory to store the plots')
parser.add_argument('-e', "--Explanation", dest='Explanation', default='Data Fakes over MC Fakes for SLT', help='Explanation of the region, to show on y axis.') # for example, "Data Fakes over MC Fakes"
parser.add_argument('-n', "--Norm", dest='Norm', default = 1, help='Whether to normalize the histograms by their integral (1 = normalize)') # for example, "Data Fakes over MC Fakes"
parser.add_argument('-c', "--Cor", dest='Cor', default = 0, help='The correlation between two histograms (between 0 and 1) to calculate the ratio sys. unc. You can set it to off then ratio unc. is not drawn.') # for example, "Data Fakes over MC Fakes"
parser.add_argument('-t', "--Tree", dest='Tree', default = 1, help='whether you are plotting from a TTree (1) or histogram (0).') # for example, "Data Fakes over MC Fakes"
args = parser.parse_args()

#To improve:
# compare files: Done
# plot histograms
# stat. unc. on ratio plot: fill.
def main():
	Files = (args.Files).split(":")

	Vars = (args.variables).split(":")

	Samples = (args.samples).split(":")

	# print(Files, Vars, Samples)
	if int(args.Tree):
		drawTrees(Files, Vars, Samples, args.Explanation)
	else:
		drawHists(Files, Vars, Samples, args.Explanation)

def drawTrees(Files, Vars, Samples, Explain):
	#hists:
	f1 = TFile.Open(Files[0], "READ")
	t1 = f1.Get("Nominal")
	v1 = Vars[0]
	s1 = Samples[0]
	
	try:
		f2 = TFile.Open(Files[1], "READ")
		t2 = f2.Get("Nominal")
		print('Comparing Files')
	except:
		t2 = t1
		print('using only one tree')
	try:
		v2 = Vars[1]
	except:
		print('using only one variable:')
		v2 = v1
		print(v1)
	try:
		s2 = Samples[1]
		print('Comparing different samples')
	except:
		print('using only one sample set')
		s2 = s1
		
	set1 = ''
	set2 = ''
	for sam in s1.split(','):
		set1 += 'sample == \"%s\" | '%sam
	for sam in s2.split(','):
		set2 += 'sample == \"%s\" | '%sam

	print(set1[:-3], set2[:-3])
	print(t1, t2)
	unit1, bin1, start1, end1 = Variables[v1][0], Variables[v1][1][0], Variables[v1][1][1], Variables[v1][1][2]
	unit2, bin2, start2, end2 = Variables[v2][0], Variables[v2][1][0], Variables[v2][1][1], Variables[v2][1][2]

	N_events1 = t1.GetEntries("(" + set1[:-3] + ") * ( OS == 1  && mBB < 150000)")
	N_events2 = t2.GetEntries("(" + set2[:-3] + ") * ( OS == 1  && mBB < 150000)")
	print('N_events1 ' + str(N_events1))
	print('N_events2 ' + str(N_events2))
	
	h1 =  TH1F("h1", Explain, bin1, start1, end1)
	h2 =  TH1F("h2", Explain, bin2, start2, end2)

	t1.Project("h1", v1, "weight * (" + set1[:-3] + ") * ( OS == 1  && mBB < 150000 )")
	t2.Project("h2", v2, "weight * (" + set2[:-3] + ") * ( OS == 1  && mBB < 150000 )")
	
	if int(args.Norm):
		setAxisStyle(h1, v1 + '   (' + unit1 +')' , " a.u. (normalised)")
		setAxisStyle(h2, v2 + '   (' + unit2 +')' , " a.u. (normalised)")
		h1.Scale(1.0 / h1.Integral())
		h2.Scale(1.0 / h2.Integral())
	else:
		setAxisStyle(h1, v1 + '   (' + unit1 +')' , " a.u. (normalized to XS*lumi)")
		setAxisStyle(h2, v2 + '   (' + unit2 +')' , " a.u. (normalized to XS*lumi)")

	hist_1over2, h_up, h_do = getSigBkgCompare(h1, h2)
	hist_1over1, _, _ = getSigBkgCompare(h1, h1)

	canvas(h1, h2, s1, s2, h_up, h_do, N_events1, N_events2, hist_1over2, hist_1over1, v1, v2, unit1, Explain)

	try:
		f2.Close()
		f1.Close()
	except:
		f1.Close()

def drawHists(Files, Vars, Samples, Explain):
	#hists:
	f1 = Files[0]
	v1 = Vars[0]
	s1 = Samples[0]
	
	try:
		f2 = Files[1]
		print('Comparing Files')
	except:
		f2 = f1
		print('using only one file')
	try:
		v2 = Vars[1]
	except:
		print('using only one variable:')
		v2 = v1
		print(v1)
	try:
		s2 = Samples[1]
		print('Comparing different samples')
	except:
		print('using only one sample set')
		s2 = s1

	f1.cd("Preselection_lowMbb150")
	f2.cd("Preselection_lowMbb150")
		
	set1 = s1.split(',')
	set2 = s2.split(',')

	h1 = gDirectory.Get(set1[0] + "_2tag_350mHH_OS_" + v1)
	h2 = gDirectory.Get(set2[0] + "_2tag_350mHH_OS_" + v2)

	for i in range(len(set1) -1):
		h1 +=  gDirectory.Get(set1[i] + "_2tag_350mHH_OS_" + v1)
	for i in range(len(set2) -1):
		h2 +=  gDirectory.Get(set2[i] + "_2tag_350mHH_OS_" + v2)

	unit1, bin1, start1, end1 = Variables[v1][0], Variables[v1][1][0], Variables[v1][1][1], Variables[v1][1][2]
	unit2, bin2, start2, end2 = Variables[v2][0], Variables[v2][1][0], Variables[v2][1][1], Variables[v2][1][2]

	N_events1 = h1.GetEntries()
	N_events2 = h2.GetEntries()
	print('N_events1 ' + str(N_events1))
	print('N_events2 ' + str(N_events2))
	
	# h1 =  TH1F("h1", Explain, bin1, start1, end1)
	# h2 =  TH1F("h2", Explain, bin2, start2, end2)
	
	if int(args.Norm):
		setAxisStyle(h1, v1 + '   (' + unit1 +')' , " a.u. (normalised)")
		setAxisStyle(h2, v2 + '   (' + unit2 +')' , " a.u. (normalised)")
		h1.Scale(1.0 / h1.Integral())
		h2.Scale(1.0 / h2.Integral())
	else:
		setAxisStyle(h1, v1 + '   (' + unit1 +')' , " a.u. (normalized to XS*lumi)")
		setAxisStyle(h2, v2 + '   (' + unit2 +')' , " a.u. (normalized to XS*lumi)")

	hist_1over2, h_up, h_do = getSigBkgCompare(h1, h2)
	hist_1over1, _, _ = getSigBkgCompare(h1, h1)
	
	canvas(h1, h2, s1, s2, h_up, h_do, N_events1, N_events2, hist_1over2, hist_1over1, v1, v2, unit1, Explain)

	try:
		f2.Close()
		f1.Close()
	except:
		f1.Close()

def canvas(h1, h2, s1, s2, h_up, h_do, N_events1, N_events2, hist_1over2, hist_1over1, v1, v2, unit1, Explain):
	h1.SetDirectory(0)
	h2.SetDirectory(0)
	h_up.SetDirectory(0)
	h_do.SetDirectory(0)
	hist_1over2.SetDirectory(0)
	hist_1over1.SetDirectory(0)
	# Build legends
	legend = TLegend(0.55,0.8,0.85,0.9)
	legend.AddEntry(h1, s1 + ': ' + str(N_events1), "l")
	legend.AddEntry(h2, s2 + ': ' + str(N_events2), "l")

	# The canvas
	c1 = TCanvas("c1", "c1", 800, 600)
	c1.cd()
	mainPad = TPad("main", "main", 0, 0.3, 1, 1)
	ratioPad = TPad("ratio", "ratio", 0, 0, 1, 0.3)
	configurePads(mainPad, ratioPad)
	mainPad.Draw()
	ratioPad.Draw()
	gStyle.SetOptStat(0) # No stats.

	mainPad.cd()
	h1.Draw()
	h2.SetLineColor(kRed)
	h2.Draw("same")
	legend.Draw("same")
	ltx = TLatex()
	ltx.SetNDC()
	# ltx.SetTextSize(0.025)

	ratioPad.cd()
	hist_1over2.Draw()
	hist_1over1.SetLineColor(kBlack)
	hist_1over1.Draw('same')
	if type(args.Cor) is int or type(args.Cor) is float:
		print('here')
		h_up.SetLineColor(kGray)
		h_up.Draw('same')
		h_do.SetLineColor(kGray)
		h_do.Draw('same')

	setRatioStyle(hist_1over2, str(v1) + unit1, "ratio    ", 0.0, 2)

	if int(args.Norm):
		plotname = args.outDir + v2 + '_Norm__' + Explain.replace(' ', '_') + ".pdf"
		c1.SaveAs(plotname, "recreate")
		mainPad.SetLogy()
		plotname = args.outDir + 'Log_' + v2 + '_Norm__' + Explain.replace(' ', '_') + ".pdf"
		c1.SaveAs(plotname, "recreate")
	else:
		print('Not Normalized')
		plotname = args.outDir + v2 + '__lumi_' + Explain.replace(' ', '_') + ".pdf"
		c1.SaveAs(plotname, "recreate")
		mainPad.SetLogy()
		plotname = args.outDir + 'Log_' + v2 + '__lumi_' + Explain.replace(' ', '_') + ".pdf"
		c1.SaveAs(plotname, "recreate")

	h1.Delete()
	h2.Delete()
	hist_1over2.Delete()
	mainPad.Close()
	ratioPad.Close()
	c1.Close()

def getSigBkgCompare(hist_sig, hist_bkg, cor = args.Cor):
	h_SoverB = hist_sig.Clone()
	h_SoverB.Reset()
	h_up = hist_sig.Clone()
	h_up.Reset()
	h_do = hist_sig.Clone()
	h_do.Reset()

	for i in range(1, hist_sig.GetNbinsX()+1):
		SoverB = 0.0
		nSig = hist_sig.GetBinContent(i)
		nBkg = hist_bkg.GetBinContent(i)
		if nBkg>1e-6:
			SoverB = nSig/nBkg
		h_SoverB.SetBinContent(i, SoverB)

		erSig = hist_sig.GetBinError(i)
		erBkg = hist_bkg.GetBinError(i)
		erRatio = abs(SoverB) * math.sqrt((erSig / nSig) ** 2 + (erBkg / nBkg) ** 2 - 2 * (cor * erSig * erBkg / (nSig * nBkg))) # default assuming the two histograms are not correlated
		h_up.SetBinContent(i, 1 + erRatio)
		h_do.SetBinContent(i, 1 - erRatio)
		# h_up.SetBinContent(i, SoverB + erRatio)
		# h_do.SetBinContent(i, SoverB - erRatio)



	return h_SoverB, h_up, h_do
#to configure the upper ("upper") and lower ("lower") Pad in a ratio plot
def configurePads(upper, lower) :
	upper.SetFrameFillColor(0)
	upper.SetFrameFillStyle(0)
	upper.SetFillColor(0)
	upper.SetFillStyle(0)
	upper.SetBottomMargin(0.03/(1-(0.4)))
	lower.SetFrameFillColor(0)
	lower.SetFrameFillStyle(0)
	lower.SetFillColor(0)
	lower.SetFillStyle(0)
	lower.SetTopMargin(0.03/(0.4))
	lower.SetBottomMargin(0.3)
	lower.SetTicky()
#to configure the style of the ratio plot for a given histogram (h_down)
def setRatioStyle(h_down, titleX, titleY, yMin, yMax):
	h_down.GetYaxis().SetNdivisions(503,kTRUE)
	h_down.GetYaxis().SetRangeUser(yMin,yMax)
	h_down.GetYaxis().SetLabelSize(0.105)
	h_down.GetYaxis().SetTitleSize(0.105)
	h_down.GetYaxis().SetTitle(titleY)          
	h_down.GetXaxis().SetTitle(titleX)
	h_down.GetYaxis().SetTitleOffset(0.5)            
	h_down.GetXaxis().SetLabelSize(0.105)
	h_down.GetXaxis().SetTitleOffset(1.4)            
	h_down.GetXaxis().SetTitleSize(0.105)
	h_down.SetTitle("")
	gPad.SetTicky()

def setAxisStyle(hist, titleX, titleY):
	hist.GetXaxis().SetLabelSize(0.05)
	hist.GetYaxis().SetLabelSize(0.05)
	if(titleY!="no"):
		hist.GetYaxis().SetTitleSize(0.05)
		hist.GetYaxis().SetTitle(titleY)
		hist.GetYaxis().SetTitleOffset(1.0) 
	if(titleY=="no"):
		hist.GetYaxis().SetTitleSize(0)
		hist.GetYaxis().SetLabelSize(0)
	if(titleX!="no"):           
		hist.GetXaxis().SetTitle(titleX) 
		hist.GetXaxis().SetTitleOffset(1.4)
		hist.GetXaxis().SetTitleSize(0.05) 
	if(titleX=="no"):
		hist.GetXaxis().SetTitleSize(0)
		hist.GetXaxis().SetLabelSize(0)
	# hist.SetTitle("")

if __name__ == "__main__":
	main()
