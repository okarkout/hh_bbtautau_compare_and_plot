#!/usr/bin/env python
import sys
from ROOT import *
import os
import math
import glob
import argparse


"""
Usage:
	python compare.py -f "<your filezzz to be compared, separated by :>" -s "<samples (or one sample) of the files (in order), separated by :, grouped by []>" -v "<variables (or one variable) from files (in order), separated by :>" -d "<name of output directory>" -e "<explanations written on the plot>"
"""
parser = argparse.ArgumentParser(description='Process output file from evaluation for plots')
parser.add_argument('-f', "--Files", dest='Files', default='/data/atlas/users/okarkout/LTT/LTT_training.root', help='your filezzz to be compared, separated by :')
parser.add_argument('-s', "--samples", dest='samples', default='Fake:ttbarFake', help='samples (or one sample) of the files (in order), separated by :, grouped by []')
parser.add_argument('-v', "--variables", dest='variables', default='MET', help='variables (or one variable) from files (in order), separated by :')
parser.add_argument('-d', "--outDir", dest='outDir', default='', help='Directory to store the plots')
parser.add_argument('-e', "--Explanation", dest='Explanation', default='', help='Explanation of the region, to show on legends, separated by :.') # for example, "2 jets, low pTV"
args = parser.parse_args()

#To improve :
# automate range of x and y axes
def main():
	# pathList = sorted(glob.glob("/data/atlas/users/okarkout/LTT/*/*.root"))
	path = "/data/atlas/users/okarkout/LTT/LTT_training.root"
	Trees = []
	for p in (args.Files).split(":"):
		print(p)
		f = TFile.Open(p, "READ")
		t = f.Get("Nominal")
		Trees.append(t)

	Vars = (args.variables).split(":")
	Samples = (args.samples).split(":")
	print(Samples)
	# f = TFile.Open(path, "READ")
	# t1 = f.Get("Nominal")
	# t1 = Trees[0]

	# h1 = TH1F("h1", "mTW", 100, 0, 300000)
	# h2 = TH1F("h2", "mTW", 100, 0, 300000)
	# h3 = TH1F("h3", "mTW", 100, 0, 300000)

	# t1.Project("h1", "mTW", "weight * (MET > 20000)")
	# t1.Project("h1", "mTW", "weight")
	# # t1.Project("h2", "mTW", "(MET > 20000)")
	# t1.Project("h3", "mTW")

	# h1.SetDirectory(0)
	# h2.SetDirectory(0)
	# h3.SetDirectory(0)
	drawTwoVars(Trees, Vars, Samples, args.Explanation)

	f.Close()


def drawTwoVars(Trees, Vars, Samples, Explain):
	#hists:
	t1 = Trees[0]
	v1 = Vars[0]
	s1 = Samples[0]
	try:
		t2 = Trees[1]
	except:
		print('using only one tree')
		t2 = t1
	try:
		v2 = Vars[1]
	except:
		print('using only one variable')
		v2 = v1
	try:
		s2 = Samples[1]
	except:
		print('using only one sample')
		s2 = s1

	
	h1 =  TH1F("h1", "Compare " + v1 + " of " + s1 +  " and " + v2 + " of " + s2, 100, 0, 300000)
	h2 =  TH1F("h2", "Compare " + v1 + " of " + s1 +  " and " + v2 + " of " + s2, 100, 0, 300000)
	t1.Project("h1", v1, "weight * (sample == \"%s\")"%s1)
	t2.Project("h2", v2, "weight * (sample == \"%s\")"%s2)

	h1.Scale(1.0 / h1.Integral())
	h2.Scale(1.0 / h2.Integral())
	hist_1over2 = getSigBkgCompare(h1, h2)

	h1.SetDirectory(0)
	h2.SetDirectory(0)
	hist_1over2.SetDirectory(0)

	# Build legends
	legend = TLegend(0.55,0.8,0.85,0.9)
	legend.AddEntry(h1, v1 + " of " + s1, "l")
	legend.AddEntry(h2, v2 + " of " + s2, "l")

	# The canvas
	c1 = TCanvas("c1", "c1", 800, 600)
	c1.cd()
	mainPad = TPad("main", "main", 0, 0.3, 1, 1)
	ratioPad = TPad("ratio", "ratio", 0, 0, 1, 0.3)
	configurePads(mainPad, ratioPad)
	mainPad.Draw()
	ratioPad.Draw()
	gStyle.SetOptStat(0) # No stats.

	mainPad.cd()
	h1.Draw()
	h2.SetLineColor(kRed)
	h2.Draw("same")
	legend.Draw("same")
	ltx = TLatex()
	ltx.SetNDC()
	ltx.SetTextSize(0.05)
	ltx.DrawLatex(0.12, 0.85, Explain)

	ratioPad.cd()
	hist_1over2.Draw()
	# hist_Significance.Draw("hist e same")

	# legratio = TLegend(0.15,0.65,0.45,0.85)
	# legratio.AddEntry(hist_1over2, "%d#times S/B"%int(1), "l")
	# # legratio.AddEntry(hist_Significance, "%d#times Significance"%int(SFSignificance), "l")
	# legratio.Draw("same")
	# # ratioMax = max(hist_1over2.GetMaximum(), hist_Significance.GetMaximum())
	# # setRatioStyle(hist_1over2, "BDT output", "S,B compare", 0., ratioMax*1.3)
	# # setRatioStyle(hist_1over2, "test output", "ratio compare", 0., hist_1over2.GetMaximum() * 1.3)
	setRatioStyle(hist_1over2, "test output", "ratio compare", 0.0, 3)

	# plotname = ""
	# if Trafo:
	#     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_TrafoD.pdf"
	# else: 
	#     plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
	# plotname = args.outDir + "/BDTouput_" + EvalRegion + "_noTrafoD.pdf"
	plotname = "output.pdf"

	c1.SaveAs(plotname, "recreate")

	h1.Delete()
	h2.Delete()
	# hist_bkg.Delete()
	hist_1over2.Delete()
	# hist_Significance.Delete()
	mainPad.Close()
	ratioPad.Close()
	c1.Close()

def getSigBkgCompare(hist_sig, hist_bkg):
	h_SoverB = hist_sig.Clone()
	h_SoverB.Reset()

	Significance_value = 0.0

	for i in range(1, hist_sig.GetNbinsX()+1):
		SoverB = 0.0
		bin_Significance = 0.0

		nSig = hist_sig.GetBinContent(i)
		nBkg = hist_bkg.GetBinContent(i)
		print("Bin%d, S: %f, B: %f"%(i, nSig, nBkg))
		if nBkg>1e-6:
			SoverB = nSig/nBkg
		print("Bin: %d, S/B: %f"%(i, SoverB))
		h_SoverB.SetBinContent(i, SoverB)
	return h_SoverB
#to configure the upper ("upper") and lower ("lower") Pad in a ratio plot
def configurePads(upper, lower) :
	upper.SetFrameFillColor(0)
	upper.SetFrameFillStyle(0)
	upper.SetFillColor(0)
	upper.SetFillStyle(0)
	upper.SetBottomMargin(0.02/(1-(0.3+0.02)))
	lower.SetFrameFillColor(0)
	lower.SetFrameFillStyle(0)
	lower.SetFillColor(0)
	lower.SetFillStyle(0)
	lower.SetTopMargin(0.02/(0.3+0.02))
	lower.SetBottomMargin(0.36)
	lower.SetTicky()
#to configure the style of the ratio plot for a given histogram (h_down)
def setRatioStyle(h_down, titleX, titleY, yMin, yMax):
	h_down.GetYaxis().SetNdivisions(503,kTRUE)
	h_down.GetYaxis().SetRangeUser(yMin,yMax)
	h_down.GetYaxis().SetLabelSize(0.105)
	h_down.GetYaxis().SetTitleSize(0.105)
	h_down.GetYaxis().SetTitle(titleY)          
	h_down.GetXaxis().SetTitle(titleX)
	h_down.GetYaxis().SetTitleOffset(0.5)            
	h_down.GetXaxis().SetLabelSize(0.105)
	h_down.GetXaxis().SetTitleOffset(1.4)            
	h_down.GetXaxis().SetTitleSize(0.105)
	h_down.SetTitle("")
	gPad.SetTicky()



if __name__ == "__main__":
	main()

